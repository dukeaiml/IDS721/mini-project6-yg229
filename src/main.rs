use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use mysql_async::Pool;
use mysql_async::prelude::*;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

#[derive(Serialize, Deserialize, Default)]
struct ResData {
    results: Vec<SubResData>,
}

#[derive(Serialize, Deserialize, Default)]
struct SubResData {
    status_message: String,
    order: String,
    original_list: Vec<i32>,
    sum: i32, // Changed from 'sorted_list' to 'sum' to reflect your request
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Directly using the provided database URL (Note: this is generally unsafe and not recommended)
    let database_url = "mysql://admin:12345678@mini5db-yg229-instance-1.cve6g20242f1.us-east-1.rds.amazonaws.com:3306/mini5db-yg229-instance-1";
    let pool = Pool::new(database_url.to_owned());
    let mut conn = pool.get_conn().await?;

    let rows: Vec<Row> = conn.query("SELECT * FROM sort").await?;

    let mut res_data: ResData = Default::default();
    for result in rows {
        let mut sub_res_data: SubResData = Default::default();
        let list: String = result.get("list").expect("Error getting 'list'");
        // Assuming 'list' is stored in a format like '1,2,3,4,5'
        let vec: Vec<i32> = list.split(',')
                                .map(|s| s.trim().parse().unwrap_or(0))
                                .collect();

        let sum: i32 = vec.iter().sum(); // Calculating sum

        sub_res_data.original_list = vec;
        sub_res_data.sum = sum; // Storing sum instead of sorting

        // Simplified the response, removed 'order' and 'status_message' for focusing on list and sum
        res_data.results.push(sub_res_data);
    }

    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(serde_json::to_string(&res_data).unwrap().into())
        .map_err(Box::new)?;
    conn.disconnect().await?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}

